package com.anderhurtado.spigot.serverping;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ServerPing extends JavaPlugin{

    @Override
    public boolean onCommand(CommandSender j,Command command,String label,String[] args){
        if(args.length==0){
            if(!(j instanceof Player)){
                j.sendMessage(ChatColor.RED+"Use: /"+label+" <Player>");
                return true;
            }
            if(!j.hasPermission("serverping.command.ping")){
                j.sendMessage(ChatColor.RED+"You don't have enough privileges for it.");
                return true;
            }new Thread(()->{
                j.sendMessage(ChatColor.AQUA+"Pinging...");
                int minecraftPing=minecraftPing((Player)j);
                long serverPing=serverPing(((Player)j).getAddress().getAddress());
                j.sendMessage(ChatColor.GREEN+"Minecraft's ping: "+(minecraftPing==-1?"ERROR":minecraftPing)+"ms.\n"+ChatColor.YELLOW+"Server's ping: "+(serverPing>-1?serverPing:(serverPing==-1?"ERROR":(serverPing==-2?"NOT REACHABLE":serverPing)))+"ms.");
            }).run();
            return true;
        }Player p=Bukkit.getPlayer(args[0]);
        if(p==null||((j instanceof Player)&&!((Player)j).canSee(p))){
            j.sendMessage(ChatColor.RED+"The player "+ChatColor.GOLD+args[0]+ChatColor.RED+" is not online!");
            return true;
        }new Thread(()->{
            j.sendMessage(ChatColor.AQUA+"Pinging...");
            int minecraftPing=minecraftPing(p);
            long serverPing=serverPing(p.getAddress().getAddress());
            j.sendMessage(ChatColor.GREEN+"Minecraft's ping of "+ChatColor.AQUA+p.getDisplayName()+ChatColor.GREEN+": "+(minecraftPing==-1?"ERROR":minecraftPing)+"ms.\n"+ChatColor.YELLOW+"Server's ping of "+ChatColor.AQUA+p.getDisplayName()+ChatColor.YELLOW+": "+(serverPing>-1?serverPing:(serverPing==-1?"ERROR":(serverPing==-2?"NOT REACHABLE":serverPing)))+"ms.");
        }).run();
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender,Command command,String alias,String[] args){
        if(args.length<2)return null;
        return new ArrayList<>();
    }

    public static int minecraftPing(Player j){
        try{
            Object obj=j.getClass().getMethod("getHandle").invoke(j);
            return(int)obj.getClass().getField("ping").get(obj);
        }catch(Exception Ex){
            Ex.printStackTrace();
            return -1;
        }
    }

    public static long serverPing(InetAddress ia){
        try{
            long start=System.currentTimeMillis();
            boolean r=ia.isReachable(10000);
            long end=System.currentTimeMillis();
            if(!r)return -2;
            return end-start;
        }catch(Exception Ex){
            Ex.printStackTrace();
            return -1;
        }
    }
}
